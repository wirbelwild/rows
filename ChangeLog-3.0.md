# Changes in Bit&Black Rows 3.0

## 3.0.3 2021-02-25

### Changed

-   Updated docblocks.

## 3.0.2 2021-02-25

### Fixed

-   Added missing spaces.

## 3.0.1 2020-12-14

### Fixed 

-   Fixed wrong handling of viewports which caused classes to be missing.

### Changed

-   Changed load order in JS to trigger the event on page load.

## 3.0.0 2020-12-11

### Changed 

-   The breakpoints have been changed:
    -   They have different names.
    -   They have different values.

-   The suffix `bb-` is now optional and __not__ set per default.

-   Removed jQuery and Browserslist as dependencies.

-   Removed folder `dist` with compiled `css` and `js` files in it.

-   Removed browser prefixes from all stylesheets.

### Added

-   Added the possibility to track viewport changes per JavaScript.