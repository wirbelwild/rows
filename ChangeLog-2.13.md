# Changes in Bit&Black Rows 2.13

## 2.13.2 2022-10-31

### Fixed

-   Replaced deprecated dependency `node-sass` with `sass`.
-   Fixed sass division.

## 2.13.1 2021-02-25

### Fixed

-   Added missing spaces.