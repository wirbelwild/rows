"use strict";
/**
 *  Bit&Black Rows
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.Viewport = void 0;
/**
 * Viewport.
 */
var Viewport = /** @class */ (function () {
    /**
     * Constructor.
     */
    function Viewport() {
        var _this = this;
        /**
         * @private
         */
        this.viewports = [
            "phone",
            "tablet-portrait",
            "tablet-landscape",
            "desktop",
            "desktop-big"
        ];
        /**
         * @param viewportName
         */
        this.callback = function (viewportName) { };
        /**
         * @private
         */
        this.mainSelector = "bb-rows-js";
        /**
         * Sets the resize callback.
         *
         * @param customCallback
         */
        this.onResize = function (customCallback) {
            _this.callback = customCallback;
            return _this;
        };
        /**
         * Returns the name of the current viewport.
         *
         * @return string
         */
        this.getViewportName = function () { return _this.viewportName; };
        window.addEventListener("resize", this.onWindowResize.bind(this));
        this.init();
    }
    /**
     * @private
     */
    Viewport.prototype.onWindowResize = function () {
        clearTimeout(this.resizeTimeout);
        this.resizeTimeout = setTimeout(this.resize.bind(this), 250);
    };
    /**
     * @private
     */
    Viewport.prototype.init = function () {
        var _this = this;
        var divMainExisting = document.querySelector(".".concat(this.mainSelector));
        if (!divMainExisting) {
            var divMain_1 = document.createElement("div");
            divMain_1.classList.add(this.mainSelector);
            var body = document.querySelector("body");
            body.appendChild(divMain_1);
            this.viewports.forEach(function (viewport) {
                var divViewport = document.createElement("div");
                divViewport.classList.add("".concat(_this.mainSelector, "__viewport"));
                divViewport.classList.add("".concat(_this.mainSelector, "__viewport--").concat(viewport));
                divViewport.dataset.bbRowViewportName = viewport;
                divMain_1.appendChild(divViewport);
            });
        }
        window.dispatchEvent(new Event("resize"));
        return this;
    };
    /**
     * @private
     */
    Viewport.prototype.resize = function () {
        var elements = document.querySelectorAll(".".concat(this.mainSelector, "__viewport"));
        var isHidden = function (element) {
            var style = window.getComputedStyle(element);
            return "none" === style.display;
        };
        for (var _i = 0, _a = elements; _i < _a.length; _i++) {
            var element = _a[_i];
            var viewport = element.dataset.bbRowViewportName;
            if (!isHidden(element)) {
                this.viewportName = viewport;
                this.callback(viewport);
                return;
            }
        }
        this.viewportName = "phone";
        this.callback("phone");
    };
    return Viewport;
}());
exports.Viewport = Viewport;
