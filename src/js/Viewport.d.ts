/**
 *  Bit&Black Rows
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
/**
 * Viewport.
 */
declare class Viewport {
    /**
     * @private
     */
    private readonly viewports;
    /**
     * @private
     */
    private resizeTimeout;
    /**
     * @param viewportName
     */
    private callback;
    /**
     * @private
     */
    private readonly mainSelector;
    /**
     * @private
     */
    private viewportName;
    /**
     * Constructor.
     */
    constructor();
    /**
     * @private
     */
    private onWindowResize;
    /**
     * @private
     */
    private init;
    /**
     * @private
     */
    private resize;
    /**
     * Sets the resize callback.
     *
     * @param customCallback
     */
    onResize: (customCallback: any) => Viewport;
    /**
     * Returns the name of the current viewport.
     *
     * @return string
     */
    getViewportName: () => string;
}
export { Viewport };
