# Changes in Bit&Black Rows 3.2

## 3.2.1 2022-02-02

### Fixed

-   Fixed missing classes.

## 3.2.0 2022-02-02

### Changed

-   For CSS there is the new viewport selector `phone-only`.
-   Dependencies have been updated.