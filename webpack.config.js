const Encore = require("@symfony/webpack-encore");

Encore
    .setOutputPath("example/build/")
    .setPublicPath("/")
    .addEntry("example", "./example/assets/example.js")
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(false)
    .enableSassLoader()
    .enableTypeScriptLoader()
    .disableSingleRuntimeChunk()
    .enableBuildNotifications()
;

module.exports = Encore.getWebpackConfig();
