[![License](https://poser.pugx.org/bitandblack/rows/license)](https://packagist.org/packages/bitandblack/rows)
[![PHP version](https://badge.fury.io/ph/bitandblack%2Frows.svg)](https://badge.fury.io/ph/bitandblack%2Frows)
[![npm version](https://badge.fury.io/js/bitandblack-rows.svg)](https://badge.fury.io/js/bitandblack-rows)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Bit&Black Rows

A small and simple CSS gutter to create rows and cells using the flexbox model.

## Table of contents

-   [Installation](#installation)

-   [Getting started](#getting-started)
    -   [Breakpoints](#breakpoints)
    -   [Spaces](#spaces)

-   [Options](#options)
    -   [Disabling the Gutter](#disabling-the-gutter)
    -   [Fit one row](#fit-one-row)
    -   [Flexible width](#flexible-width)

-   [Customization](#customization)
    -   [Namespace](#namespace)
    -   [Gutter size](#gutter-size)
    -   [Breakpoints](#breakpoints-1)
    -   [Rows number](#rows-number)
    -   [Order classes](#order-classes)
    -   [Spaces](#spaces-1)
    -   [CSS custom properties](#css-custom-properties)
    -   [Using custom classes](#using-custom-classes)

-   [Tracking the viewport](#tracking-the-viewport)

-   [Help](#help)

## Installation

This package is made for the use with [Node](https://www.npmjs.com/package/bitandblack-rows) and also for [Composer](https://packagist.org/packages/bitandblack/rows).

Add __Bit&Black Rows__ to your project by running 

-   `$ npm install bitandblack-rows` or
-   `$ composer require bitandblack/rows`

## Getting started 

__Bit&Black Rows__ uses 12 units each row per default.

Add the class `row` to an HTML container tag. The cells should get the class `row__cell`. For example:

```html
<div class="row">
    <div class="row__cell"></div>
    <div class="row__cell"></div>
    <div class="row__cell"></div>
</div>
```

Use one of the modifier classes (like `row__cell--cell2`) to define how many units a cell will take. For example: 

```html
<div class="row">
    <div class="row__cell row__cell--cell2">
        Uses 2 units.
    </div>
    <div class="row__cell row__cell--cell6">
        Uses 6 units.
    </div>
    <div class="row__cell row__cell--cell4">
        Uses 4 units.
    </div>
</div>
```

Use classes like `row__cell--rightMarginCell1` to define empty units to the left or to the right of a cell.

Use classes like `row__cell--order2` to define the sort order of cells in case you want to use breakpoints and sort differently then. 

### Breakpoints 

__Bit&Black Rows__ supports the use of breakpoints. Those are defined as: 

-   `phone`: smaller than 600px 
-   `tablet-portrait`: 600px
-   `tablet-landscape`: 900px
-   `desktop`: 1200px
-   `desktop-big`: 1800px  

Add them at the beginning of the modifiers class name. For example:

```html
<div class="row">
    <div class="row__cell row__cell--cell12 row__cell--tablet-portrait-cell6 row__cell--desktop-cell4">
        <div>12 rows per default, 6 rows when device has the resolution of a tablet in portrait or wider, 4 rows when device has a desktop resolution</div>
    </div>
    <div class="row__cell row__cell--cell12 row__cell--tablet-portrait-cell6 row__cell--desktop-cell4">
        <div>12 rows per default, 6 rows when device has the resolution of a tablet in portrait or wider, 4 rows when device has a desktop resolution</div>
    </div>
    <div class="row__cell row__cell--cell12 row__cell--tablet-portrait-cell6 row__cell--desktop-cell4">
        <div>12 rows per default, 6 rows when device has the resolution of a tablet in portrait or wider, 4 rows when device has a desktop resolution</div>
    </div>
</div>
``` 

You can also use the breakpoints in SCSS by using some mixins, for example:

```scss
@use "~bitandblack-rows/src/scss/breakpoints";

div {
    display: block;

    @include breakpoints.tablet-landscape-up {
        display: none;
    }
}
```

### Spaces

There's a smell of Bootstrap and Tailwind in __Bit&Black Rows__: You can use some classes for adding margins and paddings. There are:

-   `m` to set a `margin` from `0` to `5`
-   `mx` to set a `margin-left` and `margin-right` from `0` to `5`
-   `my` to set a `margin-top` and `margin-bottom` from `0` to `5`
-   `mt` to set a `margin-top` from `0` to `5`
-   `mr` to set a `margin-right` from `0` to `5`
-   `mb` to set a `margin-bottom` from `0` to `5`
-   `ml` to set a `margin-left` from `0` to `5`
-   `p` to set a `padding` from `0` to `5`
-   `px` to set a `padding-left` and `padding-right` from `0` to `5`
-   `py` to set a `padding-top` and `padding-bottom` from `0` to `5`
-   `pt` to set a `padding-top` from `0` to `5`
-   `pr` to set a `padding-right` from `0` to `5`
-   `pb` to set a `padding-bottom` from `0` to `5`
-   `pl` to set a `padding-left` from `0` to `5`

So to add a `margin-left` with the value of `5rem`, you need to add the class `ml-5`.

To define a margin or a padding dependent on a breakpoint, add the name of the breakpoint like `mt-tablet-portrait-5`.

If you need a negative margin, add a `n`. For example: `mt-n-5` or `mt-n-tablet-portrait-5`.

## Options 

### Disabling the Gutter 

Add the class `row--gutter-disabled` if you don't want to have a gutter at a specific place.

### Fit one row 

If you have an unknown number of cells or a number which doesn't matches your number of columns, add the class `row--fit-row`. The cells only need `row__cell` then and will result with the same width in one row.

````html
<div class="row row--fit-row">
    <div class="row__cell">
        <div>Auto size, all cells in one row</div>
    </div>
    <div class="row__cell">
        <div>Auto size, all cells in one row</div>
    </div>
    <div class="row__cell">
        <div>Auto size, all cells in one row</div>
    </div>
    <div class="row__cell">
        <div>Auto size, all cells in one row</div>
    </div>
    <div class="row__cell">
        <div>Auto size, all cells in one row</div>
    </div>
</div>
````

### Flexible width 

Sometimes the cells need a flexible width. In this case add the class `row__cell--width-flexible`. 

````html
<div class="row">
    <div class="row__cell row__cell--width-flexible">
        <div>Auto width as wide as possible</div>
    </div>
    <div class="row__cell">
        <div>Auto size fitting the content</div>
    </div>
</div>
````

## Customization

### Namespace 

You can change or remove the prefix if you want by changing the value of `$bb-rows-namespace`. 

### Gutter size

Use `$bb-rows-gutter` to define the size of your gutter. 

### Breakpoints

The breakpoints are stored in `$bb-rows-breakpoints` which you can override if you want to.

### Rows number 

The default rows number is set to `12`. You can change this by overriding the `$bb-rows-row-number`.

### Order classes 

Per default the number of `order` classes will be the same as the number of rows. If you need more of them you can define that by overriding `$bb-rows-order-number`.

### Spaces

All the space classes are using `rem` per default. The smallest unit is `1rem`. You can change this by setting `$bb-rows-spaces-unit` to a different value. 

Per default every setting has 6 steps from `0` to `5`. If you need more of them you can change the value of `$bb-rows-spaces-number`.

### CSS custom properties

Per default, __Bit&Black Rows__ uses CSS custom properties for its internal calculations. If you don't want to use them and prefer "traditional" CSS, you can disable this behaviour by setting `$bb-rows-use-custom-css-properties` to `false`.

### Using custom classes 

If you want to use custom classes for your design system, you can use mixins to access all of __Bit&Black Rows__ functionalities. 

Let's say there is a grid with a complex logic in it:

```html
<div class="row row--gutter-disabled">
    <div class="
        row__cell 
        row__cell--cell10 row__cell--leftMarginCell2
        row__cell--tablet-portrait-cell8 row__cell--tablet-portrait-leftMarginCell4
        row__cell--tablet-landscape-cell6 row__cell--tablet-portrait-leftMarginCell6
        row__cell--tablet-landscape-order2
    ">
        ...
    </div>
</div>
```

Then it could be rewritten like that:

```scss
// Bit&Black Rows needs to know the name of the child selector.
$rows-cell-selector: ".my-custom-cell";

@use "~bitandblack-rows/src/scss/variables" with (
    $bb-rows-cell-selector: $rows-cell-selector,
);
@use "~bitandblack-rows/src/scss/base";
@use "~bitandblack-rows/src/scss/breakpoints";

.my-custom-grid {
    @include base.grid;
    @include base.grid--gutter-disabled;

    #{$rows-cell-selector} {
        @include base.cell;
        @include base.cell--cell-count(10);
        @include base.cell--left-margin-cell-count(2);

        @include breakpoints.tablet-portrait-up {
            @include base.cell--cell-count(8);
            @include base.cell--left-margin-cell-count(4);
        }

        @include breakpoints.desktop-big-up {
            @include base.cell--cell-count(6);
            @include base.cell--left-margin-cell-count(6);
            @include base.cell--order(2);
        }
    }
}
```

And the HTML would be:

```html
<div class="my-custom-grid">
    <div class="my-custom-cell">
        ...
    </div>
</div>
```

## Tracking the Viewport

__Bit&Black Rows__ gives a small script to track changes of the viewport. Use it like that:

````javascript
import { Viewport } from "bitandblack-rows";

const viewport = new Viewport();

viewport.onResize((viewportName) => {
    console.log(`Viewport is currently defined as "${viewportName}"`);
});
````

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).