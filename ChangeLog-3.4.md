# Changes in Bit&Black Rows v3.4

## 3.4.1 2022-01-12

-   Added missing `flex-basis` to class `--width-flexible` to allow proper wrapping.

## 3.4.0 2022-12-27

### Added

-   Added option `$bb-rows-use-custom-css-properties` to enable or disable CSS custom properties.