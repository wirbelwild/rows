# Upgrades

## 3.x to 4.0

-   The file `src/scss/_mixins.scss` has been renamed to `src/scss/_breakpoints.scss`. If you use this file directly, you need to change the name now.
-   The file `src/scss/_private-mixins.scss` has been renamed to `src/scss/_mixins.scss`. If you use this file directly, you need to change the name now.
-   The class name of the `div` which is used to measure the viewport in JavaScript has been renamed from `bb-row-js` to `bb-rows-js`. If you use this element with its class name by yourself, you eventually need to change your selector now.

## 2.x to 3.0

### Breakpoints

The breakpoints have different names now. You need to change all the class names by your own. The changes are:

-   `x-small` to `phone`
-   `small` to `tablet-portrait`
-   `medium` to `tablet-landscape`
-   `large` to `desktop`
-   `x-large` to `desktop-big`

So if you have a class like `bb-row__cell--medium-leftMarginCell4` this should be changed to `row__cell--tablet-landscape-leftMarginCell4`.

### Prefix

The prefix `bb-` is now __not__ set per default. So if a class was `bb-row__cell` it is now `row__cell`. 

To __keep__ the prefix you need to set the variable `$bb-rows-namespace` to `bb-`. 

To __drop__ the prefix you need to update all your class names by removing `bb-`.

### Mixins and breakpoints

The mixins have also different names now. Previously there was `bb-rows-breakpoint-up()` and `bb-rows-breakpoint-down()` where you had to add the breakpoints name as parameter. Instead, all breakpoints have their own mixin now. The changes are:

-   `bb-rows-breakpoint-down(x-small)` to `phone-only`
-   `bb-rows-breakpoint-up(x-small)` to `tablet-portrait-up`
-   `bb-rows-breakpoint-down(small)` to `phone-only`
-   `bb-rows-breakpoint-up(small)` to `tablet-portrait-up`
-   `bb-rows-breakpoint-down(medium)` to `tablet-landscape-down`
-   `bb-rows-breakpoint-up(medium)` to `tablet-landscape-up`
-   `bb-rows-breakpoint-down(large)` to `desktop-down`
-   `bb-rows-breakpoint-up(large)` to `desktop-up`
-   `bb-rows-breakpoint-down(x-large)` to `desktop-big-down`
-   `bb-rows-breakpoint-up(x-large)` to `desktop-big-up`
