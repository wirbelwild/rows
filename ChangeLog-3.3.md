# Changes in Bit&Black Rows v3.3

## 3.3.1 2022-02-10

### Fixed

-   Fixed variable names.

## 3.3.0 2022-02-10

### Added

-   In addition to the existing scss variable `$bb-rows-gutter`, this value can now be set as a CSS custom property. It is called `bb-rows-gutter` and can change the gap of its cells. This can be used for different gutters at the same page.