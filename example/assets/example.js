import "./example.scss";

import { Viewport } from "../../index";

const viewport = new Viewport();

viewport
    .onResize((viewportName) => {
        console.log(`Viewport is currently defined as "${viewportName}"`);
    })
;