# Changes in Bit&Black Rows 3.1

## 3.1.0 2021-05-26

### Changed

-   Divisions have been updated to the new SASS function `math.div`.
-   Dependencies have been updated.