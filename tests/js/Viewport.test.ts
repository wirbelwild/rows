/**
 * @jest-environment jsdom
 */

import { Viewport } from "../../src/js/Viewport";

describe("Can measure Viewport", () => {
    let viewport;
    let events = {};

    beforeEach(() => {
        events = {};

        window.addEventListener = jest.fn((event, callback) => {
            events[event] = callback;
        });

        window.removeEventListener = jest.fn((event, callback) => {
            delete events[event];
        });

        viewport = new Viewport();
    });

    test("Fires callback", (done) => {
        jest.spyOn(viewport, "resize");

        viewport.onResize((viewportName) => {
            try {
                expect(viewportName).toBe("phone");
                done();
            } catch (error) {
                done(error);
            }
        });

        events["resize"]();
    });
});