# Changes in Bit&Black Rows v4.0

## 4.0.3 2023-04-04

### Fixed

-   Prevent adding multiple elements to the DOM when using class `Viewport` multiple times.

## 4.0.2 2023-02-07

### Fixed

-   Refactored the class `bb-rows-js` into the new file `_viewport-tracking.scss` to allow a separate inclusion.

## 4.0.1 2023-01-12

### Fixed

-   Added missing `flex-basis` to mixin `cell--width-flexible` to allow proper wrapping.

## 4.0.0 2023-01-02

### Changed

-   The file `src/scss/_mixins.scss` has been renamed to `src/scss/_viewports.scss`. 
-   The file `src/scss/_private-mixins.scss` has been renamed to `src/scss/_mixins.scss`.
-   The class name of the `div` which is used to measure the viewport in JavaScript has been renamed from `bb-row-js` to `bb-rows-js`. 
